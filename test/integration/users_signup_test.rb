require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get new_user_url
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "",
                                         nickname: "",
                                         password: "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
  end

  test "valid signup information" do
    get new_user_url
    assert_difference 'User.count' do
      post users_path, params: { user: { name: "Example User",
                                         nickname: "ExamU",
                                         password: "foobar",
                                         password_confirmation: "foobar" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end

require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test 'post interface' do
    log_in_as(@user)
    get root_path
    assert_select 'ul.pagination'
    assert_no_difference 'Post.count' do
      post posts_path, params: { post: { content: '' } }
    end
    # assert_select 'div#error_explanation' #have to check it
    content = 'This micropost really ties the room together'
    assert_difference 'Post.count', 1 do
      post posts_path, params: { post: { content: content } }
    end
    assert_redirected_to post_path(@user.posts.first.id)
    # get root_path
    # follow_redirect!
    # assert_match content, response.body
    # assert_select 'a', text: 'delete'
    first_post = @user.posts.page(1).first
    assert_difference 'Post.count', -1 do
      delete post_path(first_post)
    end
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end
end

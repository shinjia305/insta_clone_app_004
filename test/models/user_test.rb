require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
   @user = User.new(name: "Example User", nickname: "ExamU",
                    password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "nickname should be present" do
    @user.nickname = "     "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a" * 31
    assert_not @user.valid?
  end

  test "nickname should not be too long" do
    @user.nickname = "a" * 31
    assert_not @user.valid?
  end

  test "set of name and nickname should be uniqe" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
    duplicate_user.name = 'hogehoge'
    duplicate_user.nickname = @user.nickname
    assert duplicate_user.valid?
    duplicate_user.name = @user.name
    duplicate_user.nickname = 'hogehoge'
    assert duplicate_user.valid?
  end

  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated microposts should be destroyed" do
    @user.save
    @user.posts.create!(content: "Lorem ipsum")
    assert_difference 'Post.count', -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    lana.posts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    michael.posts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    archer.posts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end
end

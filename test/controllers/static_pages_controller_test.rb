require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Insta Clone App"
  end

  test "should get policy" do
    get policy_path
    assert_response :success
    assert_select "title", "Policy | Insta Clone App"
  end
end

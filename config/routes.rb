Rails.application.routes.draw do
  root 'static_pages#home'
  get '/policy',     to: 'static_pages#policy'
  get '/upload',     to: 'static_pages#upload'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  post '/posts/likes/:post_id/create', to: 'likes#create'
  post '/posts/likes/:post_id/destroy', to: 'likes#destroy'
  get 'auth/:provider/callback', to: 'sessions#create'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :posts, only: [:show, :create, :destroy] do
    resources :comments, only: [:create]
  end
  resources :relationships, only: [:create, :destroy]
  resources :notifications, only: :index
end

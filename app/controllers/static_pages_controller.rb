class StaticPagesController < ApplicationController
  def home
    @feed_items = current_user.feed.page(params[:page]).per(10).search(params[:search]) if logged_in?
  end

  def policy; end

  def upload
    @post = current_user.posts.build if logged_in?
  end
end

class SessionsController < ApplicationController
  def new
  end

  def create
    auth = request.env['omniauth.auth']
    if auth.present?
      user = User.find_or_create_from_auth(auth)
      session[:user_id] = user.id
      redirect_to user
    else
      user = User.find_by(name: params[:session][:name], nickname: params[:session][:nickname])
      if user && user.authenticate(params[:session][:password])
        log_in user
        # flash[:success] = "Login to the Insta Clone App!"
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        flash.now[:danger] = 'Invalid email/password combination'
        render 'new'
      end
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end

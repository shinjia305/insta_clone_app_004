class LikesController < ApplicationController
  def create
    @like = Like.new(user_id: current_user.id, post_id: params[:post_id])
    @like.save
    post = Post.find(params[:post_id])
    post.create_notification_like!(current_user)
    redirect_to post_path(post)
  end

  def destroy
    @like = Like.find_by(user_id: current_user.id, post_id: params[:post_id])
    @like.destroy
    post = Post.find(params[:post_id])
    redirect_to post_path(post)
  end
end

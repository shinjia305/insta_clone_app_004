User.create!(name: "Example User",
             nickname: "ExamU",
             password: "foobar",
             password_confirmation: "foobar",
             admin: true)

40.times do |n|
  name = Faker::Name.name
  nickname = "example-#{n+2}"
  password = "password"
  User.create!(name: name,
               nickname: nickname,
               password: password,
               password_confirmation: password,
               profile_image: File.open("./app/assets/images/default_image#{n+2}.jpeg"))
end

users = User.order(:created_at).take(40)
10.times do
  content = Faker::Lorem.sentence(word_count: 5)
  picture = Faker::Avatar.image
  users.each do |user|
    user.posts.create!(content: content,
                       picture: File.open("./app/assets/images/default_image#{user.id}.jpeg") )
  end
end

users = User.all
user  = users.first
following = users[2..30]
followers = users[3..20]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
